from VisualAid.UserInterface.VisualAidApp import VisualAidApp

# Create an instance of the application
visualAidApplication = VisualAidApp()

# Run the application
if __name__ == '__main__':
    visualAidApplication.run()
