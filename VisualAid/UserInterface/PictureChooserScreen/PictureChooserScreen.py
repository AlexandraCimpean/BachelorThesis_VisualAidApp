from kivy.uix.screenmanager import Screen
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.image import Image
from kivy.properties import StringProperty

from Models.PredictionModels import predict_image
from VisualAid.UserInterface.AppConfiguration.SettingsConfiguration import get_application_configuration


# the current selected files
current_selected_file = None


# The class representing the image label to display for predictions
class PredictionImage(Image):

    # The (last) prediction made
    prediction = StringProperty("...")

    # The name of the current model
    model = StringProperty("...")

    # The method to load in an image
    def load_image(self):
        # Only load an image if a file is selected
        if current_selected_file is not None:
            # Get the prediction of the current selected image
            prediction = predict_image(current_selected_file)
            # Update the currently displayed prediction with the new prediction
            self.prediction = prediction
            # Update the image displayed
            self.source = current_selected_file


# The class representing a picture chooser
class PictureChooserView(FileChooserIconView):

    # Get the chooser to look in the default/saved directory
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Only allow the user to select one file (picture at a time)
        self.multiselect = False
        # Initialise the directory with the last configuration
        self.update_directory()

    # The method to get the right directory to start looking in
    def update_directory(self):
        # Get the value stored for the load_path, and set the start directory to this path
        self.path = get_application_configuration().get('Picture Directories', 'load_path')

    # The method to call on a selected file/picture
    def on_selection(self, selected, touch=None):
        if len(self.selection) > 0:
            global current_selected_file
            current_selected_file = self.selection[0]


# The class representing the picture chooser screen
class PictureChooserScreen(Screen):
    pass
