from kivy.uix.screenmanager import Screen
from kivy.uix.settings import SettingsWithTabbedPanel

from VisualAid.UserInterface.SettingsConfiguration import config, read_configuration


# The class representing all settings
class AllSettings(SettingsWithTabbedPanel):

    # The method to initialise the Settings panels
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Get the Configurations Parser and load in the saved/default values
        read_configuration()
        # Add the settings panel
        self.add_json_panel('Settings', config, 'Settings.json')
        # Add the default kivy panel
        self.add_kivy_panel()

    def on_config_change(self, config_parser, section, key, value):
        # Reread the file, to update the values read by the (global) parser
        read_configuration()


# The class representing the menu screen
class MenuScreen(Screen):

    def __init__(self, **kw):
        super().__init__(**kw)
        self.allSettings = AllSettings()
