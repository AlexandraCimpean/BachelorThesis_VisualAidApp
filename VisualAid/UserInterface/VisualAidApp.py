from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
import os

from UserInterface.AppConfiguration.SettingsConfiguration import get_application_configuration
from UserInterface.PictureChooserScreen.PictureChooserScreen import PictureChooserScreen
from UserInterface.CameraScreen.CameraScreen import CameraScreen
from UserInterface.InformationScreen.InformationScreen import InformationScreen
from UserInterface.MenuScreen.MenuScreen import MenuScreen
from Models.PredictionModels import update_current_model, current_model_name


# The class representing the Visual Aid Application
class VisualAidApp(App):

    # The application screen manager
    screenManager = NotImplementedError

    # The method to build the application
    def build(self):
        # Create the application screen manager
        self.screenManager = ScreenManager()
        # Add all the screens to the screen manager
        self.screenManager.add_widget(PictureChooserScreen(name='chooser'))
        self.screenManager.add_widget(CameraScreen(name='camera'))
        self.screenManager.add_widget(InformationScreen(name='about'))
        self.screenManager.add_widget(MenuScreen(name='menu'))
        # Set the current "start" screen
        self.screenManager.current = 'menu'
        # Load in the last configured model
        last_model = get_application_configuration().get('Models', 'model')
        update_current_model(last_model)
        self.screenManager.get_screen('camera').ids.camera.model = last_model
        self.screenManager.get_screen('chooser').ids.image.model = last_model
        # Return the application screen manager
        return self.screenManager

    # The method used to set the default configuration to the provided values
    def build_config(self, config):
        path = str(os.getcwd())
        config.setdefaults('Picture Directories', {'load_path': path, 'save_path': path})
        config.setdefaults('Models', {'model': current_model_name})

    # The method to build the settings
    def build_settings(self, settings):
        # Add the settings panel
        settings.add_json_panel('Settings', self.config, 'UserInterface/AppConfiguration/Settings.json')

    # The method to listen for changes to the configuration
    def on_config_change(self, config, section, key, value):
        # Check for what setting was changed, and perform the necessary action
        changed_setting = (section, key)
        # The model was changed
        if changed_setting == ('Models', 'model'):
            update_current_model(value)
            self.screenManager.get_screen('camera').ids.camera.model = value
            self.screenManager.get_screen('chooser').ids.image.model = value
            self.screenManager.get_screen('camera').ids.camera.prediction = "..."
            self.screenManager.get_screen('chooser').ids.image.prediction = "..."
        # The load path was changed
        elif changed_setting == ('Picture Directories', 'load_path'):
            self.screenManager.get_screen('chooser').ids.picture_chooser.update_directory()
        # save_path is automatically requested when saving the image (no need to update here too)
