from kivy.uix.screenmanager import Screen


# The information screen, displaying the "about" section of the application
class InformationScreen(Screen):
    pass
