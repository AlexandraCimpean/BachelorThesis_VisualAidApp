from kivy.config import ConfigParser
from Resources.DesignConfigurations import blue_colour, white_colour, dark_blue_colour

# The file containing the Configuration parser, shared throughout the application
config = ConfigParser()


# The method returning the configuration parser of the application
def get_application_configuration():
    return config.get_configparser('app')


# The current theme
current_theme = "Light"


# Background colour
current_background_colour = white_colour
current_background_colour_scaled = list(map(lambda c: c / 255, current_background_colour)) + [1]

# Background for the picture chooser (files have white names => need colour that doesn't make the names invisible)
current_picture_chooser_colour = dark_blue_colour
current_picture_chooser_colour_scaled = list(map(lambda c: c / 255, current_picture_chooser_colour)) + [1]

# Text colour
current_text_colour = blue_colour
current_text_colour_scaled = list(map(lambda c: c / 255, current_text_colour)) + [1]


# The relative path to the resources directory
resources_directory = "../Resources/"

# Round button
current_round_down = resources_directory + current_theme + "Theme/Round_down.png"
current_round_disabled_down = resources_directory + current_theme + "Theme/Round_disabled_down.png"
current_round_normal = resources_directory + current_theme + "Theme/Round_normal.png"
current_round_disabled_normal = resources_directory + current_theme + "Theme/Round_disabled_normal.png"

# Rectangle button
current_rectangle_down = resources_directory + current_theme + "Theme/Rectangle_down.png"
current_rectangle_disabled_down = resources_directory + current_theme + "Theme/Rectangle_disabled_down.png"
current_rectangle_normal = resources_directory + current_theme + "Theme/Rectangle_normal.png"
current_rectangle_disabled_normal = resources_directory + current_theme + "Theme/Rectangle_disabled_normal.png"
