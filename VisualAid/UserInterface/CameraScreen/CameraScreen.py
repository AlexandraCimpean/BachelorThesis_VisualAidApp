from kivy.uix.screenmanager import Screen
from kivy.uix.camera import Camera
from kivy.uix.image import Image
from kivy.properties import StringProperty
from os.path import isfile, join
import time

from Models.PredictionModels import predict_image
from VisualAid.UserInterface.AppConfiguration.SettingsConfiguration import get_application_configuration


# The name of the current/last picture that was taken
current_picture_name = NotImplementedError


# The class representing the image label to display for predictions
class PredictionCameraImage(Image):

    # The method to load in an image
    def load_saved_image(self):
        # Get the save directory
        directory_path = get_application_configuration().get('Picture Directories', 'save_path')
        # Get the path to the image
        image_path = join(directory_path, current_picture_name)

        # Load in the image
        self.source = image_path


# The camera we use to take pictures
class PictureCamera(Camera):

    # The (last) prediction made
    prediction = StringProperty("...")

    # The name of the current model
    model = StringProperty("...")

    # The capture procedure to get a picture from the camera
    def capture(self):
        # Get the value stored for the save_path, and save the picture there
        directory_path = get_application_configuration().get('Picture Directories', 'save_path')
        # Compose the name of the image to save
        time_string = time.strftime("%Y-%m-%d %H:%M:%S")
        # The name of the picture
        image_name = time_string + ".png"
        # Set the current name
        global current_picture_name
        current_picture_name = image_name
        # Construct the image path
        image_path = join(directory_path, image_name)
        # Save the image
        self.export_to_png(image_path)
        # Wait for the file to be saved, so we can load it in
        while not isfile(image_path):
            continue
        # Apply predict_image to the picture
        prediction = predict_image(image_path)
        # Update the currently displayed prediction with the new prediction
        self.prediction = prediction


# The camera screen representing the interface (server as a design abstraction)
class CameraScreen(Screen):
    pass
