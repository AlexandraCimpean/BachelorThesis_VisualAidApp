# The file containing the models supported by the application

# The array of models supported (used for letting user choose)
all_models = [None, None, None, None, None, None, None]
# The array of names of the models supported
all_models_names = ["MobileNet", "NASNetMobile", "ResNet50",
                    "VisualAidNet_0.25", "VisualAidNet_0.50", "VisualAidNet_0.75", "VisualAidNet_1.00"]


# The current, default model to use
current_model = all_models[0]
# The name of the current model
current_model_name = all_models_names[0]


# The method to update the current model
def update_current_model(name):
    # Get the index for the given name
    index = all_models_names.index(name)
    # Check if the chosen model has been loaded in already
    if all_models[index] is None:
        # Create a new instance of the chosen model, based on the name
        print("Creating new model instance...")
        # MobileNet
        if index == 0:
            print("Creating new MobileNet model...")
            from Models.MobileNet.MobileNet import MobileNetModel
            all_models[index] = MobileNetModel()
        # NASNetMobile
        elif index == 1:
            print("Creating new NASNetMobile model...")
            from Models.NASNetMobile.NASNetMobile import NASNetMobileModel
            all_models[index] = NASNetMobileModel()
        # ResNet50
        elif index == 2:
            print("Creating new ResNet50 model...")
            from Models.ResNet50.ResNet50 import ResNet50Model
            all_models[index] = ResNet50Model()

        # VisualAidNet_0.25
        elif index == 3:
            print("Creating new VisualAidNet_0.25 model...")
            from Models.VisualAidNet.VisualAidNet import VisualAidNetModel
            all_models[index] = VisualAidNetModel(alpha=0.25)
        # VisualAidNet_0.50
        elif index == 4:
            print("Creating new VisualAidNet_0.50 model...")
            from Models.VisualAidNet.VisualAidNet import VisualAidNetModel
            all_models[index] = VisualAidNetModel(alpha=0.50)
        # VisualAidNet_0.75
        elif index == 5:
            print("Creating new VisualAidNet_0.75 model...")
            from Models.VisualAidNet.VisualAidNet import VisualAidNetModel
            all_models[index] = VisualAidNetModel(alpha=0.75)
        # VisualAidNet_1.00
        elif index == 6:
            print("Creating new VisualAidNet_1.00 model...")
            from Models.VisualAidNet.VisualAidNet import VisualAidNetModel
            all_models[index] = VisualAidNetModel(alpha=1.00)

    print("Updating current model...")
    # Update the current model and its name
    global current_model, current_model_name
    current_model_name = all_models_names[index]
    current_model = all_models[index]


# The method to predict an image
def predict_image(path_to_image):
    if current_model is not None:
        # Get the prediction from the current model
        prediction = current_model.get_predictions(current_model.preprocess_image(path_to_image))[0]
        class_name = prediction[0][1]
        class_probability = prediction[0][2]
        return str(class_name).replace('_', ' ') + " " + str(round(class_probability * 10000) / 100) + "%"
    else:
        return "No model initialised..."
