from keras.applications.nasnet import NASNetMobile
from keras.preprocessing import image
from keras.applications.nasnet import preprocess_input, decode_predictions
from Models.Model import Model
import numpy as np


# The class representing a NASNetMobile model
class NASNetMobileModel(Model):

    # The underlying model
    def __init__(self):
        super().__init__()
        self.model = NASNetMobile()

    # The method used to preprocess the input image
    def preprocess_image(self, picture_path):
        # Load the image
        img = image.load_img(picture_path, target_size=(224, 224))
        # Preprocess the image according to the models requirements
        preprocessed_img = image.img_to_array(img)
        preprocessed_img = np.expand_dims(preprocessed_img, axis=0)
        preprocessed_img = preprocess_input(preprocessed_img)
        # Return the preprocessed image
        return preprocessed_img

    # The method to get the predictions and decode them, given an image (to be overridden by instances of the class)
    def get_predictions(self, preprocessed_image, top=1):
        # Let the network predict
        predictions = self.model.predict(preprocessed_image)
        # Decode the predictions
        decoded_predictions = decode_predictions(predictions, top)
        # Return the decoded predictions
        return decoded_predictions
