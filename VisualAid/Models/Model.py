# The file containing an abstraction for the underlying (Keras) models


# The class encapsulating a model
class Model(object):

    # The underlying model (to be overridden by instances of the class)
    def __init__(self):
        self.model = NotImplementedError

    # The method used to preprocess the input image (to be overridden by instances of the class)
    def preprocess_image(self, picture_path):
        # Load the image
        # Preprocess the image according to the models requirements
        # Return the preprocessed image
        pass

    # The method to get the predictions and decode them, given an image (to be overridden by instances of the class)
    def get_predictions(self, preprocessed_image, top=1):
        # Let the network predict
        # Decode the predictions
        # Return the decoded predictions
        pass
