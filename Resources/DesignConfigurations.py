# Some standard colours
transparent_colour = (0, 0, 0, 0)
white_colour = (255, 255, 255)
black_colour = (0, 0, 0)
# The VUB colours
orange_colour = (255, 102, 0)
blue_colour = (0, 52, 154)
# Darker blue
dark_blue_colour = (0, 20, 75)

# Light Theme
light_theme_border = blue_colour
light_theme_fill = white_colour

light_theme_border_down = white_colour
light_theme_fill_down = blue_colour

light_theme_border_dimmed = (150, 150, 150)
light_theme_fill_dimmed = (200, 200, 200)
