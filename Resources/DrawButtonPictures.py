# This is a file used for generating the button pictures for the application
from Resources.DesignConfigurations import *
from PIL import Image, ImageDraw

# The directories used
resources_directory = "../../../Resources/"
light_theme_directory = "LightTheme/"

# The file names of the pictures
light_theme_round_normal = light_theme_directory + "Round_normal.png"
light_theme_round_disabled_normal = light_theme_directory + "Round_disabled_normal.png"
light_theme_round_down = light_theme_directory + "Round_down.png"
light_theme_round_disabled_down = light_theme_directory + "Round_disabled_down.png"

light_theme_rectangle_normal = light_theme_directory + "Rectangle_normal.png"
light_theme_rectangle_disabled_normal = light_theme_directory + "Rectangle_disabled_normal.png"
light_theme_rectangle_down = light_theme_directory + "Rectangle_down.png"
light_theme_rectangle_disabled_down = light_theme_directory + "Rectangle_disabled_down.png"


# The image size (in pixels) of all the buttons
picture_width = 1000
picture_height = 1000

# Some useful values for drawing the circles
x1 = 0
y1 = 0
x2 = picture_width * 0.075
y2 = picture_height * 0.075
x3 = picture_width * 0.15
y3 = picture_height * 0.15
x4 = picture_width * 0.225
y4 = picture_height * 0.225


# The procedure to draw a circle for our buttons
def draw_circle(draw, x, y, colour):
    # Draw the circle
    draw.ellipse((x, y, picture_width - x, picture_height - y), fill=colour)


# The procedure to draw a rectangle for our buttons
def draw_rectangle(draw, x, y, colour):
    # Draw the rectangle
    draw.rectangle((x, y, picture_width - x, picture_height - y), fill=colour)


# Procedure to draw a round button
def draw_round_button(filename, border_colour, fill_colour):
    # Draw a transparent picture
    image = Image.new("RGBA", (picture_width, picture_height), (0, 0, 0, 0))
    # Get the drawing context for the image
    draw = ImageDraw.Draw(image)
    # Draw the circles
    draw_circle(draw, x1, y1, border_colour)
    draw_circle(draw, x2, y2, fill_colour)
    draw_circle(draw, x3, y3, border_colour)
    draw_circle(draw, x4, y4, fill_colour)
    # Save the picture
    image.save(filename, "PNG")


# Procedure to draw a rectangle button
def draw_rectangle_button(filename, border_colour, fill_colour):
    # Draw a transparent picture
    image = Image.new("RGBA", (picture_width, picture_height), (0, 0, 0, 0))
    # Get the drawing context for the image
    draw = ImageDraw.Draw(image)
    # Draw the rectangles
    #draw_rectangle(draw, x1, y1, fill_colour)
    draw_rectangle(draw, x2, y2, border_colour)
    draw_rectangle(draw, x3, y3, fill_colour)
    #draw_rectangle(draw, x4, y4, border_colour)
    # Save the picture
    image.save(filename, "PNG")


# Main function
if __name__ == '__main__':
    # Light Theme
    # Round buttons
    draw_round_button(light_theme_round_normal, light_theme_border, light_theme_fill)
    draw_round_button(light_theme_round_disabled_normal, light_theme_border_dimmed, light_theme_fill_dimmed)
    draw_round_button(light_theme_round_down, light_theme_border_down, light_theme_fill_down)
    draw_round_button(light_theme_round_disabled_down, light_theme_border_dimmed, light_theme_fill_dimmed)

    # Rectangle buttons
    draw_rectangle_button(light_theme_rectangle_normal, light_theme_border, light_theme_fill)
    draw_rectangle_button(light_theme_rectangle_disabled_normal, light_theme_border_dimmed, light_theme_fill_dimmed)
    draw_rectangle_button(light_theme_rectangle_down, light_theme_border_down, light_theme_fill_down)
    draw_rectangle_button(light_theme_rectangle_disabled_down, light_theme_border_dimmed, light_theme_fill_dimmed)
